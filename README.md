# Mandelbrot 2010

<img src="https://gitlab.com/FrVi/Mandelbrot-2010/raw/master/exe/Screenshot/HD.jpg" alt="Drawing" width="512"/>
<br />

This program displays the Mandelbrot set.

You can **left-click** on a part of the set to zoom on it and **right-click** to zoom out.

Press **Return** to take a screenshot and **F9** to take a HD screenshot.

Press **Space** to switch between normal and fast mode.

**F1** displays an antialiased version of the current viewpoint.

**F2** increases the maximal number of iterations per pixel. 

This increases the computation cost and the image quality.

**F3** decreases the maximal number of iterations per pixel.

This is the 2010 version of the program, it uses 2 threads.

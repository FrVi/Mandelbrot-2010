#ifndef MANDELBROT_H
#define MANDELBROT_H

	#include <stdlib.h> 
	#include <stdio.h> 
	#include <SDL/SDL.h> 
	
	#define taillereel 1024
	#define taille2 (taillereel/2)
	#define taille4 (taillereel/4)
	#define zoomfacteur 2
	#define COLOR 0x000000
	
	typedef struct Coordonnees Coordonnees;

	struct Coordonnees
	{
		double x; 
		double y;    
	};

	class Mandelbrot
	{
		public :

			static int LeftHD(void* data);
			static int Left(void* data);
			static int LeftS(void* data);
			static int LeftR(void* data);

			Mandelbrot(double xmin, double xmax, int zoomlevel,SDL_Surface *ecran);               
			
			void Zoom(SDL_Event event,  double zoomfact,int mode);
			void HDbmp(); 
			void setPixel4(int x,int y,Uint32 coul);         
			void setPixel(int x,int y,Uint32 coul);
			void set_T(double facteur);
			void Slow();
			void Normal();
			void Rapide();
			void Normal_part(int x1,int x2);
			void Slow_part(int x1,int x2,int y1, int y2);
			void Rapide_part(int x1,int x2);                 
			void teststatistique(double zoomfact);  
			void Box(int x, int x2, int y,int y2);
 			bool Init(Uint32* tab); 
 			void bmp();
 			void info(int zoomlvl); 
 			void FillRect();
             
		private :
			SDL_Surface* ecran;
  			double xmin;
  			double xmax;
  			double ymin;
  			double ymax;
  			double largeur;

   			int zoomlevel; 
   
			SDL_Thread* DrawLeft;
			int *status;
			void* T0;

			double T;

   			Uint32* tableau;
			Uint32* pix;
			int w;

			SDL_PixelFormat *fmt;
      };

#endif

#include "mandelbrot.h"

int main(int argc, char *argv[])
{
	int taille = taillereel;

	SDL_Event event;

	SDL_Surface *ecran = NULL;

	SDL_Init(SDL_INIT_VIDEO);
	ecran = SDL_SetVideoMode(taille, taillereel, 32, SDL_HWSURFACE);
	SDL_WM_SetCaption("Mandelbrot (30 mai 2009)", NULL);

	int tempsPrecedent = 0, tempsActuel = 0;

	int mode = 1;//mode de dessin: 1 == propre, 2 == rapide
	double xmin = -2;      double xmax = 2;
	int zoomlevel = 5;

	FILE* fichier = NULL;

	fopen_s(&fichier, "test.txt", "w");

	int x, y, j;

	int xdemi = (taillereel) / (2 * zoomfacteur);  int ydemi = (taillereel) / (2 * zoomfacteur);

	double echelle[100] = { 32 * (xmax - xmin) / taillereel, 0 };
	double largeur = (xmax - xmin) / taillereel;

	for (j = 1; j < 99; j++) { echelle[j] = echelle[j - 1] / 2; }

	SDL_EnableKeyRepeat(1000, 100);
	Uint32 tableau[2000] = { 0 };

	Mandelbrot mandel(xmin, xmax, zoomlevel, ecran);
	bool cond = mandel.Init(tableau);

	tempsPrecedent = SDL_GetTicks();
	mandel.info(zoomlevel);
	mandel.Normal();
	SDL_UpdateRect(ecran, 0, 0, 0, 0);

	tempsActuel = SDL_GetTicks();

	fprintf(fichier, "aaa %ld \n", sizeof(long double));

	SDL_PollEvent(&event);
	if (cond == true)
	{
		while (event.type != SDL_QUIT)
		{
			SDL_WaitEvent(&event);

			if (event.type != SDL_QUIT && event.type == SDL_MOUSEBUTTONUP)
			{
				if (event.button.button == SDL_BUTTON_LEFT)
				{

					if (zoomlevel - 49 < 0)
					{
						zoomlevel++;
						SDL_GetMouseState(&x, &y);

						if (mode == 1){ mandel.Box(x - xdemi, x + xdemi, y - ydemi, y + ydemi); }
						else{ mandel.Box(x - xdemi / 2, x + xdemi / 2, y - ydemi / 2, y + ydemi / 2); }

						SDL_UpdateRect(ecran, 0, 0, 0, 0);
						mandel.Zoom(event, zoomfacteur, mode);
						mandel.teststatistique(zoomfacteur);
						largeur = echelle[++j];
					}
				}
				else if (event.button.button == SDL_BUTTON_RIGHT)
				{
					if (zoomlevel > 0)
					{
						zoomlevel--;
						mandel.Zoom(event, (double)1 / zoomfacteur, mode);
						mandel.teststatistique((double)1 / zoomfacteur);
						largeur = echelle[--j];
					}
				}
				else if (event.button.button == SDL_BUTTON_MIDDLE){ mandel.Zoom(event, 1, mode); }

				if (mode == 1)
				{
					mandel.info(zoomlevel);
					mandel.Normal();
					SDL_UpdateRect(ecran, 0, 0, 0, 0);
				}
				else
				{
					mandel.info(zoomlevel);
					mandel.Rapide();
					SDL_UpdateRect(ecran, taille4, taille4, taille2, taille2);
				}
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_RETURN)  { mandel.info(zoomlevel);  mandel.bmp(); SDL_Delay(10); }
				else if (event.key.keysym.sym == SDLK_F9)
				{
					tempsPrecedent = SDL_GetTicks();
					mandel.HDbmp();
					tempsActuel = SDL_GetTicks();
					fprintf(fichier, "\n %ld \n", tempsActuel - tempsPrecedent);
				}
				else if (event.key.keysym.sym == SDLK_SPACE)
				{
					mode = mode == 1 ? 2 : 1;
					if (mode == 1)
					{
						mandel.info(zoomlevel);
						mandel.Normal();
					}
					else
					{
						mandel.info(zoomlevel);
						mandel.FillRect(); mandel.Rapide();
					}
					SDL_UpdateRect(ecran, 0, 0, 0, 0);
				}
				else if (event.key.keysym.sym == SDLK_F1)
				{
					mode = 1;
					mandel.set_T(2.0);
					mandel.Slow();
					mandel.info(zoomlevel);
					mandel.set_T(0.5);
				}
				else if (event.key.keysym.sym == SDLK_F2)
				{
					mandel.set_T(1.045);
					mandel.info(zoomlevel);
				}
				else if (event.key.keysym.sym == SDLK_F3)
				{
					mandel.set_T(1 / 1.045);
					mandel.info(zoomlevel);
				}
			}
		}
	}

	SDL_Quit();
	fclose(fichier);
	return EXIT_SUCCESS;
}

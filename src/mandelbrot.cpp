#include "mandelbrot.h"

Mandelbrot* Mand;

int Mandelbrot::LeftHD(void* data)
{
	Mand->Slow_part(0, 4096, 0, 2048);
	return 0;
}

//_________________________________________________________________\\

int Mandelbrot::Left(void* data)
{
	Mand->Normal_part(0, taille2);
	return 0;
}

//_________________________________________________________________\\

int Mandelbrot::LeftS(void* data)
{
	Mand->Slow_part(0, taille2, 0, taillereel);
	return 0;
}

//_________________________________________________________________\\

int Mandelbrot::LeftR(void* data)
{
	Mand->Rapide_part(taille4, taille4 * 2);
	return 0;
}

//_________________________________________________________________\\

void Mandelbrot::FillRect()
{
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
}

//_________________________________________________________________\\

Mandelbrot::Mandelbrot(double xmin2, double xmax2, int zoomlevel2, SDL_Surface *ecran2)
{
	pix = (Uint32*)(ecran2->pixels);
	w = ecran2->w;
	fmt = ecran2->format;

	Mand = this;

	T = 76;
	T0 = NULL;
	status = NULL;

	xmin = xmin2;
	xmax = xmax2;
	ymin = xmin2;
	ymax = xmax2;

	zoomlevel = zoomlevel2;
	ecran = ecran2;
	largeur = (xmax2 - xmin2) / taillereel;
}

//_________________________________________________________________\\

bool Mandelbrot::Init(Uint32* tab)
{

	FILE* fichier = NULL;
	fopen_s(&fichier, "couleurs.txt", "r");

	if (fichier != NULL)
	{
		tableau = tab;
		Uint8 i, k, l;
		int j;

		for (j = 0; j < 2000; j++)
		{
			fscanf_s(fichier, "%ld", &i);
			fscanf_s(fichier, "%ld", &k);
			fscanf_s(fichier, "%ld", &l);
			tab[j] = SDL_MapRGBA(ecran->format, i, k, l, 255);
		}

		return true;
	}
	else 
	{ 
		return false; 
	}
}

//_________________________________________________________________\\

void Mandelbrot::Zoom(SDL_Event event, double zoomfact, int mode)
{
	double ycentre, xcentre;
	double distance = (xmax - xmin) / (2 * zoomfact);
	if (mode == 1)
	{
		xcentre = xmin + event.button.x*(largeur);
		ycentre = ymin + event.button.y*(largeur);
	}
	else
	{
		xcentre = xmin + 2 * (event.button.x - taillereel / 4)*(largeur);
		ycentre = ymin + 2 * (event.button.y - taillereel / 4)*(largeur);
	}

	xmin = xcentre - distance;
	xmax = xcentre + distance;
	ymin = ycentre - distance;
	ymax = ycentre + distance;

	largeur = largeur / zoomfact;
}

//_________________________________________________________________\\

void Mandelbrot::setPixel(int x, int y, Uint32 coul)
{
	*(pix + x + y * w) = coul;
}

//_________________________________________________________________\\

void Mandelbrot::Normal()
{
	DrawLeft = SDL_CreateThread(Left, T0);//partie gauche dans un thread s�par�
	Normal_part(taille2, taillereel);//partie droite
	SDL_WaitThread(DrawLeft, status);
}

//_________________________________________________________________\\

void Mandelbrot::Slow()
{
	DrawLeft = SDL_CreateThread(LeftS, T0);//partie gauche dans un thread s�par�
	Slow_part(taille2, taillereel, 0, taillereel);//partie droite
	SDL_WaitThread(DrawLeft, status);
	SDL_UpdateRect(ecran, 0, 0, 0, 0);
}

//_________________________________________________________________\\

void Mandelbrot::Rapide()
{
	DrawLeft = SDL_CreateThread(LeftR, T0);//partie gauche dans un thread s�par�
	Rapide_part(2 * taille4, taille4 * 3);//partie droite
	SDL_WaitThread(DrawLeft, status);
}

//_________________________________________________________________\\

void Mandelbrot::Normal_part(int x1, int x2)
{
	Coordonnees z, Z, Z2, p;

	int x, y, t;
	int j = 0;
	int Tc = (int)T;

	for (x = x1; x <= x2 - 1; x++)
	{
		for (y = 0; y <= taillereel - 1; y++)
		{
			t = -Tc;
			p.x = (xmin)+(x)*(largeur);     p.y = -(y)*(largeur)-(ymin);
			z.x = p.x;                        z.y = p.y;
			Z2.x = z.x * z.x;                Z2.y = z.y * z.y;

			while (Z2.x + Z2.y - 4 <= 0 && t < 0)
			{
				Z.x = Z2.x + p.x - Z2.y;     Z.y = p.y + 2 * z.x * z.y;
				z.x = Z.x;                      z.y = Z.y;
				Z2.x = z.x * z.x;              Z2.y = z.y * z.y;
				t++;
			}
			if (t == 0){ setPixel(x, y, 0xFFFFFF); j++; }
			else        { setPixel(x, y, tableau[t + Tc]); }
		}
	}
}

//_________________________________________________________________\\
 
void Mandelbrot::Rapide_part(int x1, int x2)
{
	Coordonnees z, Z, Z2;
	int x, y, t;
	double X, Y;

	int Tc = (int)T + 1;

	for (x = x1; x <= x2 - 1; x = x + 2)
	{
		for (y = taille4; y <= 3 * taille4; y = y + 2)
		{
			t = -Tc;
			X = xmin + 2 * (largeur)*(x - taille4);
			Y = ymin + 2 * (largeur)*(y - taille4);
			z.x = X;
			z.y = Y;
			Z2.x = z.x * z.x;
			Z2.y = z.y * z.y;

			while (Z2.x + Z2.y - 4 <= 0 && t < 0)
			{
				Z.x = Z2.x + X - Z2.y; //z.x 2 div par 10 , de meme z.y
				Z.y = Y + 2 * z.x * z.y; //z.x * z.y div par 10
				z.x = Z.x;
				z.y = Z.y;
				Z2.x = z.x * z.x;
				Z2.y = z.y * z.y;
				t++;
			}

			if ((t == 0))
			{

				setPixel4(x, y, 0xFFFFFF);

			}
			else
			{
				setPixel4(x, y, tableau[t + Tc]);
			}
		}
	}
}

//_________________________________________________________________\\

void Mandelbrot::setPixel4(int x, int y, Uint32 coul)
{
	Uint32* lol = pix + x + y * w;

	*(lol) = coul;
	*(lol + 1) = coul;
	*(lol + w) = coul;
	*(lol + 1 + w) = coul;
}

//_________________________________________________________________\\

void Mandelbrot::teststatistique(double zoomfact)
{
	Coordonnees z, Z, Z2, p;
	int x, y, t;

	int Tc = 1 + (int)(T);
	double tableauC_x[512] = { 0 }; //point initial
	double tableauC_y[512] = { 0 };
	double tableauF_x[512] = { 0 }; //valeur final
	double tableauF_y[512] = { 0 };
	int i = 0;
	int nbr = 0;
	int nbr2 = 0;
	for (x = taillereel / 32; x <= taillereel - 1; x = (int)(x + taillereel / 16))
	{
		for (y = taillereel / 32; y <= taillereel - 1; y = (int)(y + taillereel / 16))
		{
			t = -Tc;
			p.x = (xmin)+(x)*(largeur);
			p.y = -(y)*(largeur)-(ymin);
			z.x = p.x;
			z.y = p.y;
			Z2.x = z.x * z.x;
			Z2.y = z.y * z.y;

			while (Z2.x + Z2.y - 4 <= 0 && t < 0)
			{
				Z.x = Z2.x + p.x - Z2.y; //z.x 2 div par 10 , de meme z.y
				Z.y = p.y + 2 * z.x * z.y; //z.x * z.y div par 10
				z.x = Z.x;
				z.y = Z.y;
				Z2.x = z.x * z.x;
				Z2.y = z.y * z.y;

				t++;
			}

			if ((t == 0)){
				tableauC_x[i] = p.x;
				tableauC_y[i] = p.y;
				tableauF_x[i] = z.x;
				tableauF_y[i] = z.y;
				i++;
			}
		}
	}

	nbr = i - 1;
	for (i = 0; i <= nbr; i++)
	{
		t = -Tc;
		p.x = tableauC_x[i];
		p.y = tableauC_y[i];
		z.x = tableauF_x[i];
		z.y = tableauF_y[i];
		Z2.x = z.x * z.x;
		Z2.y = z.y * z.y;

		while (Z2.x + Z2.y - 4 <= 0 && t < 0)
		{
			Z.x = Z2.x + p.x - Z2.y; //z.x 2 div par 10 , de meme z.y
			Z.y = p.y + 2 * z.x * z.y; //z.x * z.y div par 10
			z.x = Z.x;
			z.y = Z.y;
			Z2.x = z.x * z.x;
			Z2.y = z.y * z.y;

			t++;
		}
		if ((t == 0)){ nbr2++; }
	}

	if (zoomfact > 1)
	{
		if (i - nbr2 > 70){ T = Tc*1.141; }
		else if (i - nbr2 > 40){ T = Tc*1.092; }
		else { T = 1.045*Tc; }
	}
	else
	{
		if (i - nbr2 > 70){ T = Tc*1.092; }
		else if (i - nbr2 > 40){ T = (double)Tc; }
		else { T = Tc / 1.045; }
	}

	if (T > 2000.0){ T = 2000.0; }
	else if (T <= 1.0){ T = 1.0; }
}

//_________________________________________________________________\\

void Mandelbrot::Slow_part(int x1, int x2, int y1, int y2)
{
	int x, y;
	int t;
	Uint32 table[4] = { 0 };
	Uint8 r[4] = { 0 };
	Uint8 g[4] = { 0 };
	Uint8 b[4] = { 0 };
	Uint8 a;
	int i;
	double ab, od;
	double X, Y;
	Coordonnees z, Z;
	int Tc = (int)T;

	for (x = x1; x <= x2 - 1; x++)
	{
		for (y = y1; y <= y2 - 1; y++)
		{
			i = 0;
			for (od = 0.0; od <= 0.5; od = od + 0.5)
			{
				for (ab = 0.0; ab <= 0.5; ab = ab + 0.5)
				{
					t = 1 - Tc;
					X = xmin + (x + ab)*largeur;
					Y = -ymin - (y + od)*largeur;
					z.x = X;
					z.y = Y;


					while (z.x * z.x + z.y * z.y - 4 <= 0 && t < 0)
					{
						Z.x = z.x * z.x - z.y * z.y + X;
						Z.y = Y + 2 * z.x * z.y;
						z.x = Z.x;
						z.y = Z.y;
						t++;
					}

					if ((t == 0))  { table[i] = (Uint32)0xFFFFFF; }
					else { table[i] = (Uint32)tableau[t + Tc - 1]; }

					i++;
				}
			}

			for (i = 0; i < 4; i++)
			{
				SDL_GetRGBA(table[i], fmt, &(r[i]), &(g[i]), &(b[i]), &a);
			}

			r[0] = (Uint8)((r[0] + r[1] + r[2] + r[3]) / 4);
			g[0] = (Uint8)((g[0] + g[1] + g[2] + g[3]) / 4);
			b[0] = (Uint8)((b[0] + b[1] + b[2] + b[3]) / 4);

			*((Uint32*)(ecran->pixels) + x + y * ecran->w) = (Uint32)(SDL_MapRGBA(fmt, r[0], g[0], b[0], 255));
		}
	}
}


//_________________________________________________________________\\

void Mandelbrot::Box(int x, int x2, int y, int y2)
{
	Uint32 coul = COLOR;
	x = (x < 0) ? 0 : x;   //evite de dessiner en dehors de l'ecran
	x2 = (x2 > taillereel - 1) ? taillereel - 1 : x2;
	y = (y < 0) ? 0 : y;
	y2 = (y2 > taillereel - 1) ? taillereel - 1 : y2;

	int i;

	for (i = x; i <= x2; i++)
	{
		*(pix + i + y *w) = coul;
		*(pix + i + y2 *w) = coul;
	}

	for (i = y; i <= y2; i++)
	{
		*(pix + x + i *w) = coul;
		*(pix + x2 + i *w) = coul;
	}
}

//_________________________________________________________________\\

void Mandelbrot::info(int zoomlvl)
{
	zoomlevel = zoomlvl;
	double ycentre, xcentre;
	xcentre = (xmin + xmax) / 2;
	ycentre = (ymin + ymax) / 2;
	char chaine[] = "                                                                                                                            ";
	sprintf_s(chaine, "Mandelbrot  (%.17lf ,%.17lf) %ld %ld", xcentre, ycentre, (int)T, zoomlevel);
	SDL_WM_SetCaption(chaine, NULL);
}

//_________________________________________________________________\\

void Mandelbrot::HDbmp()
{
	SDL_Surface *rectangle = NULL;
	SDL_Surface *rectangle2 = ecran;
	rectangle = SDL_CreateRGBSurface(SDL_HWSURFACE, 4096, 4096, 32, 0, 0, 0, 0);
	ecran = rectangle;

	double l = largeur;
	largeur = largeur / 4;

	DrawLeft = SDL_CreateThread(LeftHD, T0);
	Slow_part(0, 4096, 2048, 4096);
	SDL_WaitThread(DrawLeft, status);
	char chaine[200] = { 0 };
	sprintf_s(chaine, "Screenshot/HD(%.17lf,%.17lf) %ld %ld.bmp", (xmax + xmin) / 2, (ymin + ymax) / 2, (int)T, zoomlevel);
	SDL_SaveBMP(ecran, chaine);
	largeur = l;
	ecran = rectangle2;
	SDL_FreeSurface(rectangle);
}

//_________________________________________________________________\\

void Mandelbrot::bmp()
{
	char chaine[100] = { 0 };
	sprintf_s(chaine, "Screenshot/(%.17lf,%.17lf) %ld %ld.bmp", (xmax + xmin) / 2, (ymin + ymax) / 2, (int)T, zoomlevel);
	SDL_SaveBMP(ecran, chaine);
}

void Mandelbrot::set_T(double facteur)
{
	T = T*facteur;
	if (T > 2000.0){ T = 2000.0; }
	else if (T <= 1.0){ T = 1.0; }
}
